# RMIT Research Collaboration Network

As one of the key measure for demonstrating level of interdisciplinary research collaborations within the university, Transport@RMIT Network did a scientific collaboration study based on the transport related published papers which had an author affiliated with RMIT university.

Therefore, a co-authorship analysis has been conducted to demonstrate the level of collaboration within different author and authors has been classified based on their collaborations into ?? groups.

After network analysis and classification, two maps was generated: First map illustrates the network considering other co-authors from outside the RMIT (first figure) and the second map only considers the authors who are affiliated to RMIT (second figure).

## Network of all co-authors

In the first outcome, all co-authors were considered (whether affiliated to RMIT or not). Figure 1 illustrates this network.

**To access the interactive network please click [here](http://www.vosviewer.com/vosviewer.php?map=https://bitbucket.org/jafshin/rmit_transport_network/raw/master/NetworkWithExternalLinks/MapGeneral.txt&network=https://bitbucket.org/jafshin/rmit_transport_network/raw/master/NetworkWithExternalLinks/NetworkGeneral.txt)**

![First Figure](OutcomeWithAllAuthors.jpg)
Figure 1. Co-authorship network for all authors.

## Network of all co-authors

In the second outcome, only authors affiliated to RMIT were considered. Figure 2 illustrates this network.

**To access the interactive network please click [here](http://www.vosviewer.com/vosviewer.php?map=https://bitbucket.org/jafshin/rmit_transport_network/raw/master/NetworkWithoutExternalLinks/mapRMITOnly.txt&network=https://bitbucket.org/jafshin/rmit_transport_network/raw/master/NetworkWithoutExternalLinks/NetworkRMITOnly.txt)**

![Second Figure](OutcomeRMITOnly.jpg)
Figure 2. Co-authorship network for RMIT affiliated authors.
